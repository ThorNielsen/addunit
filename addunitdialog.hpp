#ifndef ADDUNITDIALOG_H
#define ADDUNITDIALOG_H

//(*Headers(AddUnitDialog)
#include <wx/combobox.h>
#include <wx/checkbox.h>
#include <wx/dialog.h>
#include <wx/button.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

#include <cbproject.h>

class AddUnitDialog : public wxDialog
{
public:

    AddUnitDialog(wxWindow* parent, cbProject* proj);
    virtual ~AddUnitDialog();

    inline wxString SourcePathName() const { return m_fsrcpath; }
    inline wxString HeaderPathName() const { return m_fsrcpath; }
    inline wxString TestPathName() const { return m_fsrcpath; }
    inline wxArrayString PathNames() const
    {
        wxArrayString a;
        if (m_fsrcpath != _("")) a.push_back(m_fsrcpath);
        if (m_fhdrpath != _("")) a.push_back(m_fhdrpath);
        if (m_ftstpath != _("")) a.push_back(m_ftstpath);
        return a;
    }

protected:

    //(*Identifiers(AddUnitDialog)
    //*)

private:

    //(*Declarations(AddUnitDialog)
    wxTextCtrl* HeaderName;
    wxButton* CreateButton;
    wxTextCtrl* ImplName;
    wxTextCtrl* ClassName;
    wxButton* CancelButton;
    wxCheckBox* CreateImpl;
    wxStaticText* StaticText1;
    wxTextCtrl* TestName;
    wxStaticText* StaticText3;
    wxCheckBox* CreateTest;
    wxStaticText* StaticText2;
    wxCheckBox* CreateHeader;
    wxCheckBox* LowercaseFilenames;
    wxComboBox* ModuleName;
    //*)

    //(*Handlers(AddUnitDialog)
    void OnClassNameText(wxCommandEvent& event);
    void OnModuleNameSelected(wxCommandEvent& event);
    void OnHeaderNameText(wxCommandEvent& event);
    void OnImplementationNameText(wxCommandEvent& event);
    void OnTestNameText(wxCommandEvent& event);
    void OnCreateHeaderClick(wxCommandEvent& event);
    void OnCreateImplClick(wxCommandEvent& event);
    void OnCreateTestClick(wxCommandEvent& event);
    void OnLowercaseClick(wxCommandEvent& event);
    void OnCancelButtonClick(wxCommandEvent& event);
    void OnCreateButtonClick(wxCommandEvent& event);
    //*)

    bool IsClassNameValid(wxString name);
    void UpdateClassName(wxString name);
    void EnableOK(bool enable = true);
    void SetFileNames(wxString base, bool tolowercase = true);
    void EnableTextbox(wxTextCtrl*, bool enable = true);
    inline void DisableTextbox(wxTextCtrl* t, bool disable = true)
    {
        EnableTextbox(t, !disable);
    }
    inline void DisableOK(bool disable = true) { EnableOK(!disable); }
    void Error(wxString description);
    wxFileName FindFolder(wxFileName BasePath, wxArrayString names);
    wxFileName FindSourceCodeFolder(wxFileName BasePath);
    wxFileName FindTestCodeFolder(wxFileName BasePath);

    wxArrayString GetSubFolders(wxFileName folder);

    wxString GenerateHeader(wxString module, wxArrayString nspaces,
                            bool hasSource);
    wxString GenerateSource(wxString includename, wxArrayString nspaces);
    wxString GenerateTest(wxString includename, wxArrayString nspaces);

    wxArrayString m_modules;
    wxString m_classname;
    wxString m_sourcename;
    wxString m_headername;
    wxString m_testname;
    wxFileName m_sourcepath;
    wxFileName m_testpath;
    bool m_gensource;
    bool m_genheader;
    bool m_gentest;
    bool m_okenabled;

    wxString m_fsrcpath;
    wxString m_fhdrpath;
    wxString m_ftstpath;



    DECLARE_EVENT_TABLE()
};

#endif
