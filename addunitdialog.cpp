#include "addunitdialog.hpp"
#include <logmanager.h>
#include <manager.h>
#include <wx/dir.h>
#include <wx/dirdlg.h>
#include "listdir.hpp"
#include <algorithm>
#include <editormanager.h>

//(*InternalHeaders(AddUnitDialog)
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(AddUnitDialog)
//*)

BEGIN_EVENT_TABLE(AddUnitDialog,wxDialog)
	//(*EventTable(AddUnitDialog)
	//*)
END_EVENT_TABLE()


void Log(std::string s, Logger::level lvl = Logger::info);
void Log(wxString s, Logger::level lvl = Logger::info);

AddUnitDialog::AddUnitDialog(wxWindow* parent, cbProject* proj)
{
	//(*Initialize(AddUnitDialog)
	if (!wxXmlResource::Get()->LoadObject(this,parent,_T("addunitdialog"),_T("wxDialog")))
        Manager::Get()->GetLogManager()->Log(_("Couldn't load resources!"));
	StaticText1 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT1"));
	ClassName = (wxTextCtrl*)FindWindow(XRCID("ID_CLASSNAME"));
	StaticText2 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT2"));
	ModuleName = (wxComboBox*)FindWindow(XRCID("ID_MODULENAME"));
	CreateHeader = (wxCheckBox*)FindWindow(XRCID("ID_CREATEHEADER"));
	HeaderName = (wxTextCtrl*)FindWindow(XRCID("ID_HEADERNAME"));
	CreateImpl = (wxCheckBox*)FindWindow(XRCID("ID_CREATEIMPL"));
	ImplName = (wxTextCtrl*)FindWindow(XRCID("ID_IMPLNAME"));
	CreateTest = (wxCheckBox*)FindWindow(XRCID("ID_CREATETEST"));
    LowercaseFilenames = (wxCheckBox*)FindWindow(XRCID("ID_LOWERCASE"));
	TestName = (wxTextCtrl*)FindWindow(XRCID("ID_TESTNAME"));
	StaticText3 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT3"));
	CreateButton = (wxButton*)FindWindow(XRCID("wxID_OK"));
	CancelButton = (wxButton*)FindWindow(XRCID("wxID_CANCEL"));

	Connect(XRCID("ID_CLASSNAME"),wxEVT_COMMAND_TEXT_UPDATED,(wxObjectEventFunction)&AddUnitDialog::OnClassNameText);
	Connect(XRCID("ID_MODULENAME"),wxEVT_COMMAND_COMBOBOX_SELECTED,(wxObjectEventFunction)&AddUnitDialog::OnModuleNameSelected);
	Connect(XRCID("ID_LOWERCASE"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&AddUnitDialog::OnLowercaseClick);
	Connect(XRCID("ID_CREATEHEADER"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&AddUnitDialog::OnCreateHeaderClick);
	Connect(XRCID("ID_HEADERNAME"),wxEVT_COMMAND_TEXT_UPDATED,(wxObjectEventFunction)&AddUnitDialog::OnHeaderNameText);
	Connect(XRCID("ID_CREATEIMPL"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&AddUnitDialog::OnCreateImplClick);
	Connect(XRCID("ID_IMPLNAME"),wxEVT_COMMAND_TEXT_UPDATED,(wxObjectEventFunction)&AddUnitDialog::OnImplementationNameText);
	Connect(XRCID("ID_CREATETEST"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&AddUnitDialog::OnCreateTestClick);
	Connect(XRCID("ID_TESTNAME"),wxEVT_COMMAND_TEXT_UPDATED,(wxObjectEventFunction)&AddUnitDialog::OnTestNameText);
	Connect(XRCID("wxID_OK"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&AddUnitDialog::OnCreateButtonClick);
	Connect(XRCID("wxID_CANCEL"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&AddUnitDialog::OnCancelButtonClick);
	//*)


	m_sourcepath = FindSourceCodeFolder(proj->GetBasePath());
	m_testpath = FindTestCodeFolder(proj->GetBasePath());

    m_modules = GetSubFolders(m_sourcepath);
    std::sort(m_modules.begin(), m_modules.end());
    for (size_t i = 0; i < m_modules.size(); ++i)
    {
        ModuleName->Append(m_modules[i]);
        //Manager::Get()->GetLogManager()->Log(arr[i], LogManager::app_log, Logger::warning);
    }
	m_gensource = true;
	m_genheader = true;
	m_gentest = false;
	m_classname = _("");
	DisableOK();
	DisableTextbox(TestName);
	CreateTest->SetValue(false);
}

AddUnitDialog::~AddUnitDialog()
{
	//(*Destroy(AddUnitDialog)
	//*)
}

wxString generateNamespaceBegin(wxArrayString namespaces)
{
    wxString result = _("");
    for (auto ns = namespaces.begin(); ns != namespaces.end(); ++ns)
    {
        result << _("namespace ") << *ns << _("\n{\n\n");
    }
    return result;
}

wxString generateNamespaceEnd(wxArrayString namespaces)
{
    wxString result = _("");
    for (auto ns = namespaces.rbegin(); ns != namespaces.rend(); ++ns)
    {
        result << _("} // namespace ") << *ns << _("\n\n");
    }
    return result;
}

wxString AddUnitDialog::GenerateTest(wxString includename,
                                     wxArrayString nspaces)
{
    wxString endl = _("\n");
    wxString contents;
    if (includename != _(""))
    {
        contents << _("#include \"") << includename << _("\"") << endl;
    }
    contents << _("#include \"unittest.hpp\"") << endl;
    contents << endl;
    if (nspaces.size())
    {
        contents << generateNamespaceBegin(nspaces);
        contents << endl << endl;
        contents << generateNamespaceEnd(nspaces);
    }
    return contents;
}

wxString AddUnitDialog::GenerateSource(wxString includename,
                                       wxArrayString nspaces)
{
    wxString endl = _("\n");
    wxString indent = _("    ");
    wxString contents;
    if (includename != _(""))
    {
        contents << _("#include \"") << includename << _("\"") << endl;
    }
    contents << endl;
    if (nspaces.size())
    {
        contents << generateNamespaceBegin(nspaces);
    }
    contents << m_classname << _("::") << m_classname << _("()") << endl;
    contents << _("{") << endl;
    contents << _("}") << endl;
    contents << endl;
    // todo:virtual
    contents << m_classname << _("::~") << m_classname << _("()") << endl;
    contents << _("{") << endl;
    contents << _("}") << endl;
    contents << endl;
    if (nspaces.size())
    {
        contents << generateNamespaceEnd(nspaces);
    }
    return contents;
}

wxString AddUnitDialog::GenerateHeader(wxString module, wxArrayString nspaces,
                                       bool hasSource)
{
    wxString guardword = module.Upper();
    if (guardword.size() && guardword[guardword.length() - 1] != '/')
        guardword << _("/");
    guardword << m_headername.Upper();
    guardword.Replace(_("."), _("_"));
    guardword.Replace(_("/"), _("_"));
    guardword << _("_INCLUDED");
    wxString endl = _("\n");
    wxString indent = _("    ");
    wxString contents;
    contents << _("#ifndef ") << guardword << endl;
    contents << _("#define ") << guardword << endl;
    contents << endl;
    if (nspaces.size())
    {
        contents << generateNamespaceBegin(nspaces);
    }
    contents << _("class ") << m_classname << endl; // : xyz otherclass
    contents << _("{") << endl;
    contents << _("public:") << endl;
    wxString cons = hasSource ? _(";") : _(" {}");
    contents << indent << m_classname << _("()") << cons << endl;
    contents << indent << /* virtual */ _("~")
             << m_classname << _("()") << cons << endl;
    //contents += endl;
    //contents += _("protected:") + endl;
    contents << _("private:") << endl;
    //contents += endl;
    contents << _("};") << endl;
    contents << endl;
    if (nspaces.size())
    {
        contents << generateNamespaceEnd(nspaces);
    }
    contents << _("#endif // ") << guardword << endl;
    contents << endl;
    return contents;
}

wxFileName AddUnitDialog::FindFolder(wxFileName BasePath, wxArrayString names)
{
    wxDir dir(BasePath.GetLongPath());
    for (size_t i = 0; i < names.size(); ++i)
    {
        if (dir.Exists(names[i]))
        {
            BasePath.AppendDir(names[i]);
            return BasePath;
        }
    }
    return BasePath;
}

/// TODO: Find the directory that contains the source code by looking for stuff
/// in the following order:
/// "src/" or "source/"
/// Folder containing main.cpp
/// Folder containing the most .cpp files whose name doesn't include 'test'.
/// Folder containing the most .cpp files.
wxFileName AddUnitDialog::FindSourceCodeFolder(wxFileName BasePath)
{
    wxArrayString names;
    names.push_back(_("src"));
    names.push_back(_("source"));
    return FindFolder(BasePath, names);
}

/// TODO: Search in the same way as FindSourceCodeFolder should do.
wxFileName AddUnitDialog::FindTestCodeFolder(wxFileName BasePath)
{
    wxArrayString names;
    names.push_back(_("test"));
    names.push_back(_("tests"));
    names.push_back(_("testing"));
    return FindFolder(BasePath, names);
}

wxArrayString AddUnitDialog::GetSubFolders(wxFileName folder)
{
    listdir ld;
    wxDir dir(folder.GetLongPath());
    dir.Traverse(ld, wxEmptyString, wxDIR_DIRS);
    for (wxString& s : ld.subdirs())
    {
        if (s.find(folder.GetLongPath()) != wxString::npos)
        {
            s = s.substr(folder.GetLongPath().size());
        }
    }
    return ld.subdirs();
}

bool AddUnitDialog::IsClassNameValid(wxString name)
{
    if (!name.size()) return false;
    for (size_t i = 0; i < name.size(); ++i)
    {
        wxChar c = name[i];
        if (!i && isdigit(c)) return false;
        if (!(isalnum(c) || c == '_')) return false;
    }
    return true;
}

void AddUnitDialog::EnableOK(bool enable)
{
    m_okenabled = enable;
    if (enable)
    {
        CreateButton->Enable();
        CreateButton->SetDefault();
    }
    else
    {
        CreateButton->Disable();
        CancelButton->SetDefault();
    }
}

void AddUnitDialog::SetFileNames(wxString base, bool tolowercase)
{
    if (tolowercase) base.LowerCase();
    if (m_gensource && base != _("")) ImplName->SetValue(base + _(".cpp"));
    else ImplName->SetValue(_(""));
    if (m_genheader && base != _("")) HeaderName->SetValue(base + _(".hpp"));
    else HeaderName->SetValue(_(""));
    if (m_gentest && base != _("")) TestName->SetValue(_("test_") + base + _(".cpp"));
    else TestName->SetValue(_(""));
}

void AddUnitDialog::UpdateClassName(wxString name)
{
    m_classname = name;
    if (IsClassNameValid(m_classname))
    {
        SetFileNames(m_classname, LowercaseFilenames->GetValue());
        EnableOK();
    }
    else
    {
        SetFileNames(m_classname == _("") ? _("") : _("<INVALID>"), false);
        DisableOK();
    }
}

void AddUnitDialog::EnableTextbox(wxTextCtrl* t, bool enable)
{
    SetFileNames(m_classname, LowercaseFilenames->GetValue());
    if (enable)
    {
        t->Enable();
    }
    else
    {
        //t->SetValue(_(""));
        t->Disable();
    }
}

void AddUnitDialog::OnClassNameText(wxCommandEvent& event)
{
    UpdateClassName(ClassName->GetValue());
}

void AddUnitDialog::OnModuleNameSelected(wxCommandEvent& event)
{
}

void AddUnitDialog::OnHeaderNameText(wxCommandEvent& event)
{
    m_headername = HeaderName->GetValue();
}

void AddUnitDialog::OnImplementationNameText(wxCommandEvent& event)
{
    m_sourcename = ImplName->GetValue();
}

void AddUnitDialog::OnTestNameText(wxCommandEvent& event)
{
    m_testname = TestName->GetValue();
}

void AddUnitDialog::OnCreateHeaderClick(wxCommandEvent& event)
{
    m_genheader = CreateHeader->GetValue();
    EnableTextbox(HeaderName, m_genheader);
}

void AddUnitDialog::OnCreateImplClick(wxCommandEvent& event)
{
    m_gensource = CreateImpl->GetValue();
    EnableTextbox(ImplName, m_gensource);
}

void AddUnitDialog::OnCreateTestClick(wxCommandEvent& event)
{
    m_gentest = CreateTest->GetValue();
    EnableTextbox(TestName, m_gentest);
}

void AddUnitDialog::OnLowercaseClick(wxCommandEvent& event)
{
    OnClassNameText(event);
}

void AddUnitDialog::OnCancelButtonClick(wxCommandEvent& event)
{
    if (m_classname != _("") && !m_okenabled)
    {
        auto response = cbMessageBox(_("You have entered an incorrect class"
                                       "name. Did you really wish to cancel?"),
                                     _("Cancel?"),
                                     wxYES_NO | wxYES_DEFAULT,
                                     this);
        if (response != wxID_YES) return;
    }
    EndModal(wxID_CANCEL);
}

bool CreateFile(wxFileName path, wxString contents)
{
    wxFile f;
    if (!f.Create(path.GetLongPath()))
    {
        return false;
    }
    f.Write(contents);
    return true;
}

wxArrayString split(wxString str, wxChar at)
{
    wxArrayString matches;
    size_t pos = 0;
    while (pos < str.size())
    {
        size_t newpos = str.find_first_of(at, pos);
        if (newpos != pos)
        {
            if (newpos == wxString::npos)
            {
                matches.push_back(str.substr(pos));
            }
            else
            {
                matches.push_back(str.substr(pos, newpos - pos));
            }
        }
        pos = ((newpos == wxString::npos) ? newpos : newpos + 1);
    }
    return matches;
}

void AddUnitDialog::Error(wxString description)
{
    cbMessageBox(description, _("Error"), wxOK | wxICON_ERROR, this);
}

void AddUnitDialog::OnCreateButtonClick(wxCommandEvent& event)
{
    if (!IsClassNameValid(ClassName->GetValue()))
    {
        Error(_("You somehow succeeded in clicking OK even though it was"
                " disabled. Please enter a correct class name and report"
                " this bug. Thank you."));
        return;
    }
    wxString dir = ModuleName->GetValue();
    wxArrayString folders = split(dir, '/');

    wxFileName srcpath = m_sourcepath;
    wxFileName testpath = m_testpath;

    for (auto& f : folders)
    {
        srcpath.AppendDir(f);
        testpath.AppendDir(f);
    }

    bool srcdir = srcpath.DirExists();
    bool testdir = testpath.DirExists();

    Log(srcdir ? _("srcdir exists") : _("src doesn't"));
    Log(srcdir ? _("testdir exists") : _("src doesn't"));


    if ((!srcdir && (m_genheader || m_gensource)) ||
        (!testdir && m_gentest))
    {
        wxString message = _("The module \"");
        message += dir;
        message += _("\" doesn't seem to exist. Create it?");
        bool createDirectory = cbMessageBox(message,
                                            _("New module"),
                                            wxYES_NO,
                                            this) == wxID_YES;
        if (!createDirectory) return;
        if (!(srcdir && (m_genheader || m_gensource)))
        {
            CreateDirRecursively(srcpath.GetLongPath());
        }
        if (!(testdir && m_gentest))
        {
            CreateDirRecursively(testpath.GetLongPath());
        }
    }

    if (dir.size() && dir[dir.size() - 1] != (wxChar)'/')
        dir += _("/");

    wxString headerIncludeName = _("");

    wxArrayString nspaces = folders;

    if (m_genheader)
    {
        wxFileName headerpath = srcpath;
        headerpath.SetFullName(m_headername);
        if (headerpath.FileExists())
        {
            Error(_("The file \"") +
                  headerpath.GetFullPath() +
                  _("\" already exists."));
            return;
        }
        wxFile hdr;
        hdr.Open(headerpath.GetFullPath(), wxFile::OpenMode::write);
        if (!hdr.IsOpened())
        {
            Error(_("The file \"") +
                  headerpath.GetFullPath() +
                  _("\" couldn't be opened for writing."));
            return;
        }
        hdr.Write(GenerateHeader(dir, nspaces, m_gensource));
        headerIncludeName = dir + m_headername;
        m_fhdrpath = headerpath.GetFullPath();
        Manager::Get()->GetEditorManager()->Open(m_fhdrpath);
    }

    if (m_gensource)
    {
        wxFileName sourcepath = srcpath;
        sourcepath.SetFullName(m_sourcename);
        if (sourcepath.FileExists())
        {
            Error(_("The file \"") +
                  sourcepath.GetFullPath() +
                  _("\" already exists."));
            return;
        }
        wxFile src;
        src.Open(sourcepath.GetFullPath(), wxFile::OpenMode::write);
        if (!src.IsOpened())
        {
            Error(_("The file \"") +
                  sourcepath.GetFullPath() +
                  _("\" couldn't be opened for writing."));
            return;
        }
        src.Write(GenerateSource(headerIncludeName, nspaces));
        m_fsrcpath = sourcepath.GetFullPath();
        Manager::Get()->GetEditorManager()->Open(m_fsrcpath);
    }


    if (m_gentest)
    {
        wxFileName testfilepath = testpath;
        testfilepath.SetFullName(m_testname);
        if (testfilepath.FileExists())
        {
            Error(_("The file \"") +
                  testfilepath.GetFullPath() +
                  _("\" already exists."));
            return;
        }
        wxFile test;
        test.Open(testfilepath.GetFullPath(), wxFile::OpenMode::write);
        if (!test.IsOpened())
        {
            Error(_("The file \"") +
                  testfilepath.GetFullPath() +
                  _("\" couldn't be opened for writing."));
            return;
        }
        test.Write(GenerateTest(headerIncludeName, nspaces));
        m_ftstpath = testfilepath.GetFullPath();
        Manager::Get()->GetEditorManager()->Open(m_ftstpath);
    }

    EndModal(wxID_OK);
}
