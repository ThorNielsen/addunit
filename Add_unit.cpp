#include <sdk.h> // Code::Blocks SDK
#include <configurationpanel.h>
#include "Add_unit.h"
#include <logmanager.h>
#include <projectmanager.h>
#include <cbproject.h>
#include "addunitdialog.hpp"

// Register the plugin with Code::Blocks.
// We are using an anonymous namespace so we don't litter the global one.
namespace
{
    PluginRegistrant<Add_unit> reg(_T("Add_unit"));
    auto menuId = wxNewId();
}

BEGIN_EVENT_TABLE(Add_unit, cbPlugin)
    EVT_MENU(menuId, Add_unit::OnMenuItemClick)
END_EVENT_TABLE()

void Log(std::string s, Logger::level lvl = Logger::info)
{
    Manager::Get()->GetLogManager()->Log(wxString::FromAscii(s.c_str()),
                                         LogManager::app_log, lvl);
}

void Log(wxString s, Logger::level lvl = Logger::info)
{
    Manager::Get()->GetLogManager()->Log(s, LogManager::app_log, lvl);
}

// constructor
Add_unit::Add_unit()
{
    // Make sure our resources are available.
    // In the generated boilerplate code we have no resources but when
    // we add some, it will be nice that this code is in place already ;)
    if(!Manager::LoadResource(_T("Add_unit.zip")))
    {
        NotifyMissingFile(_T("Add_unit.zip"));
    }
}

// destructor
Add_unit::~Add_unit()
{
}

void Add_unit::BuildMenu(wxMenuBar* menuBar)
{
    auto pos = menuBar->FindMenu(_("File"));
    if (pos == wxNOT_FOUND)
    {
        Log(_("File menu entry not found!"), Logger::failure);
        return;
    }
    wxMenu* FileMenu = menuBar->GetMenu(pos);
    auto NewPos = FileMenu->FindItem(_("New"));
    if (NewPos == wxNOT_FOUND)
    {
        Log(_("New entry not found!"), Logger::failure);
        return;
    }
    wxMenuItem* NewFileMenu = FileMenu->FindItem(NewPos);
    m_NewFileMenu = NewFileMenu ? NewFileMenu->GetSubMenu() : nullptr;
    if (m_NewFileMenu)
    {
        m_NewFileMenu->Insert(1, menuId, _("Unit..."));
    }
}

void Add_unit::OnMenuItemClick(wxCommandEvent& event)
{
    ProjectManager* pman = Manager::Get()->GetProjectManager();
    cbProject* project = pman->GetActiveProject();
    AddUnitDialog dialog(Manager::Get()->GetAppWindow(), project);
    PlaceWindow(&dialog);
    if (dialog.ShowModal() == wxID_OK)
    {
        wxArrayInt projectTargets;
        wxArrayString files = dialog.PathNames();
        if (files.size())
        {
            pman->AddMultipleFilesToProject(files, project, projectTargets);
            pman->GetUI().RebuildTree();
        }
    }
    else
    {
        // Cancel
    }
}

void Add_unit::OnAttach()
{
    // do whatever initialization you need for your plugin
    // NOTE: after this function, the inherited member variable
    // m_IsAttached will be TRUE...
    // You should check for it in other functions, because if it
    // is FALSE, it means that the application did *not* "load"
    // (see: does not need) this plugin...
    Log(_("Loading AddUnit."));
}

void Add_unit::OnRelease(bool appShutDown)
{
    // do de-initialization for your plugin
    // if appShutDown is true, the plugin is unloaded because Code::Blocks is being shut down,
    // which means you must not use any of the SDK Managers
    // NOTE: after this function, the inherited member variable
    // m_IsAttached will be FALSE...
    if (!appShutDown) Log(_("Unloading AddUnit."));
}
