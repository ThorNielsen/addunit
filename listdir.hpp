#ifndef LISTDIR_HPP_INCLUDED
#define LISTDIR_HPP_INCLUDED

#include <wx/dir.h>

class listdir : public wxDirTraverser
{
public:
    wxArrayString& subdirs() { return dirs; }
    inline virtual wxDirTraverseResult OnFile(const wxString& file)
    {
        return wxDIR_CONTINUE;
    }
    inline virtual wxDirTraverseResult OnDir(const wxString& dir)
    {
        dirs.push_back(dir);
        return wxDIR_CONTINUE;
    }
private:
    wxArrayString dirs;
};

#endif // LISTDIR_HPP_INCLUDED
